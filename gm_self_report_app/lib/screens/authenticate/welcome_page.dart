import 'package:flutter/material.dart';

import 'file:///C:/Users/ofira/Desktop/alonApps/gm-self-report-app/gm_self_report_app/lib/shared/constants.dart';
import 'package:gm_self_report_app/shared/wave_clip.dart';

class WelcomePage extends StatelessWidget {

  // final Function switchLogin;
  // final Function switchRegister;
  // WelcomePage({ this.switchLogin, this.switchRegister });

  @override
  Widget build(BuildContext context) {
    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () { Navigator.pushNamed(context, '/authenticate'); }, //switchLogin,
        child: Text("Login",
            textAlign: TextAlign.center,
            style: TEXT_STYIE.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    final registerButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Colors.white,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () { Navigator.pushNamed(context, '/authenticate'); }, // switchRegister,
        child: Text("Register",
            textAlign: TextAlign.center,
            style: TEXT_STYIE.copyWith(
                color: Colors.black26, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
        body:  ClipPath(
          clipper: WaveClipper(),
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0, 0.8],
                colors: <Color>[
                  Colors.lightBlue[200],
                  Colors.white//lightBlue[50]
                ],
              ),
            ),
          ),
        ),

      //   Center(
      //     child: Padding(
      //       padding: const EdgeInsets.all(36.0),
      //       child: Column(
      //           children: <Widget>[
      //             Text(
      //               "welcome",
      //                 style: TextStyle(
      //                   color: Colors.black,
      //                   fontSize: 30.0,
      //                 ),
      //             ),
      //             SizedBox(
      //               height: 155.0,
      //               child: Image.asset(
      //                 "assets/logo.png",
      //                 fit: BoxFit.contain,
      //               ),
      //             ),
      //             Expanded(child: SizedBox()),
      //             loginButon,
      //             Divider(
      //               height: 80,
      //               color: Colors.black87,
      //             ),
      //             registerButon
      //
      //         ]
      //       )
      //   ),
      // )
    );
      //Container();
  }
}

