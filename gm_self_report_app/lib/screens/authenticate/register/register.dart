import 'package:flutter/material.dart';
import 'package:gm_self_report_app/shared/constants.dart';


class Register extends StatefulWidget {
  final Function switchLogin;
  Register({ this.switchLogin });

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  String email;
  String username;

  String password;
  String confirm;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 155.0,
                    child: Image.asset(
                      "assets/logo.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                  SizedBox(height: 35.0),

                  MyTextField(
                      hintText: "email",
                      onChangedFnc: (String str) {
                        setState(() { email = str;});
                      }
                  ),
                  SizedBox(height: 25.0),
                  MyTextField(
                      hintText: "username",
                      onChangedFnc: (String str) {
                        setState(() { username = str;});
                      }
                  ),
                  SizedBox(height: 25.0),
                  Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: MyTextField(
                            hintText: "password",
                            onChangedFnc: (String str) {
                              setState(() { password = str;});
                            }
                        ),
                      ),
                      SizedBox(width: 12),
                      Expanded(
                        flex: 3,
                        child:MyTextField(
                            hintText: "confirm",
                            onChangedFnc: (String str) {
                              setState(() { confirm = str;});
                            }
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 25.0),
                  MyButton(
                      buttonText: "Register",
                      buttonFnc: () {print (email);}
                  ),
                  SizedBox(height: 30.0,),
                ],
              ),
            )
        )
    );
  }
}
