import 'package:flutter/material.dart';

import 'package:gm_self_report_app/shared/constants.dart';


class Login extends StatefulWidget {

  final Function switchRegister;
  Login({ this.switchRegister });

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  String email = "";
  String pass = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 155.0,
                  child: Image.asset(
                    "assets/logo.png",
                    fit: BoxFit.contain,
                  ),
                ),
                SizedBox(height: 45.0),

                MyTextField(
                    hintText: "email",
                    onChangedFnc: (String str) {
                      setState(() { email = str;});
                    }
                ),
                SizedBox(height: 25.0),

                MyTextField(
                    hintText: "pass",
                    onChangedFnc: (String str) {
                      setState(() { pass = str;});
                    }
                ),
                SizedBox(height: 35.0,),

                MyButton(
                    buttonText: "login",
                    buttonFnc: () {print (email + '|' + pass);}
                ),
                SizedBox(height: 30.0,),

                Divider( height: 40, color: Colors.black87, ),
                Container(
                    padding: EdgeInsets.all(16),

                    child: Row(
                      children: [
                        Text("Don’t have an account? "),
                        Text('Signup now', style: TextStyle(fontWeight: FontWeight.bold))
                      ],
                    )
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}