
import 'package:flutter/material.dart';

import 'package:gm_self_report_app/models/jsonObjects/questairJson.dart';
import 'package:gm_self_report_app/models/jsonObjects/answersJson.dart';
import 'package:gm_self_report_app/screens/home/questair/begin_task.dart';
import 'package:gm_self_report_app/screens/home/questair/perform_questionnaire.dart';

import 'package:gm_self_report_app/utils/http_connection.dart';

class PerformSelfReport extends StatefulWidget {
  Questair questionnaires;
  Answers answers;

  PerformSelfReport()
  {
    // get the Questionnaires from the server
    questionnaires = getQuestionnaire();
    answers = Answers(uid: questionnaires.uid, questionnaires: []);
  }

  @override
  _PerformSelfReportState createState() => _PerformSelfReportState();
}

class _PerformSelfReportState extends State<PerformSelfReport> {
  bool userConfirmation = false;
  int questionnaireIndex = 0;

  void startNextQuestionnaire()
  {
    setState(() {
      questionnaireIndex += 1;
      userConfirmation = false;
    });
  }

  void userConfirming ()
  {
    setState(() {
      userConfirmation = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    // check if there are moor Questionnaire
    if (widget.questionnaires.questionnaires.length <= questionnaireIndex)
    {
      // if all the Questionnaires are filled out
      sendAns(widget.answers);
      return Scaffold(
        body: Text("Questionnaires summary!"),
      );
    }
    else {
      // Begin task Confirmation
      if (!userConfirmation) {
        // wait for the Confirmation of the user
        return BeginTask(
            questionnaireInstructions: widget.questionnaires.questionnaires[questionnaireIndex].instructions,
            userConfirming: userConfirming);
      }
      else
      {
        // add place to save ans from Questionnaire
        widget.answers.questionnaires.add(
            QuestionnaireAns(
                uid: widget.questionnaires.questionnaires[questionnaireIndex].uid,
                answers: [] ));

        // Begin this Questionnaire
        return PerformQuestionnaire(
          questionnaire: widget.questionnaires.questionnaires[questionnaireIndex],
          questionnaireAns: widget.answers.questionnaires[questionnaireIndex],
          startNextQuestionnaire: startNextQuestionnaire
        );
      }
    }
  }
}
