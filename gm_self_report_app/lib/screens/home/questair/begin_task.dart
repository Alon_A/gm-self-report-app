import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:gm_self_report_app/shared/constants.dart';

class BeginTask extends StatelessWidget {
  final String questionnaireInstructions;
  final Function userConfirming;

  BeginTask({this.questionnaireInstructions, this.userConfirming});

  @override
  Widget build(BuildContext context) {
    // wait for the Confirmation of the user
    return Scaffold(
      appBar: MyAppBar(title: "Self Report",),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Expanded(
              child: Center(
                child: Container(
                  child: Text(
                      questionnaireInstructions),
                ),
              )
          ),
          Padding(
              padding: const EdgeInsets.all(40),
              child: MyButton(
                  buttonFnc: userConfirming,
                  buttonText: "Begin Task"
              )
          ),
        ],
      ),
    );
  }
}
