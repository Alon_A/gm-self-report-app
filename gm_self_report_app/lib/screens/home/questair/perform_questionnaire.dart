
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:gm_self_report_app/models/jsonObjects/questairJson.dart';
import 'package:gm_self_report_app/models/jsonObjects/answersJson.dart';

import 'package:gm_self_report_app/shared/constants.dart';

class PerformQuestionnaire extends StatefulWidget {
  final Questionnaire questionnaire;
  QuestionnaireAns questionnaireAns;

  final Function startNextQuestionnaire;

  PerformQuestionnaire({ this.questionnaire, this.questionnaireAns, this.startNextQuestionnaire });

  @override
  _PerformQuestionnaireState createState() => _PerformQuestionnaireState();
}

class _PerformQuestionnaireState extends State<PerformQuestionnaire> {
  int questionIndex = 0;
  Question thisQuestion;

  void nextQuestion ()
  {
    setState(() {
      questionIndex += 1;
    });
  }

  Row answersPlace (List<Answer> answersOptions)
  {
    return Row(
      children: answersOptions.map<Widget>(
              (answerOption) {
            return  Expanded(
              child: Column(
                children: [
                  MaterialButton(
                      onPressed: () {setState(() {
                        widget.questionnaireAns.markAns (questionIndex, answerOption.value);
                        nextQuestion();
                      });
                      },

                      color: widget.questionnaireAns.checkIfMarked(questionIndex, answerOption.value) ?
                      Colors.cyan[100] : Colors.white,

                      padding: EdgeInsets.all(16),
                      shape: CircleBorder(side: BorderSide(
                          color: Colors.black, width: 1.0, style: BorderStyle.solid)
                      ),
                      child: Text("${answerOption.value}")
                  ),
                  Text(answerOption.text)
                  // Expanded(child: Text(answerOption.text))
                ],
              ),
            );
          }).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {

    if (0 > questionIndex)
    { questionIndex = 0; }

    else if (questionIndex >= widget.questionnaire.questions.length)
    {
      //questionIndex = widget.questionnaire.questions.length - 1;

      // The end of this questionnaire
      return Scaffold(
        appBar: MyAppBar(title: "Self Report",),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
                child: Center(
                  child: Container(
                    child: Text(
                        "The end of this questionnaire"),
                  ),
                )
            ),
            Padding(
                padding: const EdgeInsets.all(40),
                child: MyButton(
                    buttonFnc: widget.startNextQuestionnaire,
                    buttonText: "go to the next"
                )
            ),
          ],
        ),
      );
    }

    // set the question by the Index for this state
    thisQuestion = widget.questionnaire.questions[questionIndex];

    return Scaffold(
      appBar: MyAppBar(title: "Self Report"),

      body: Padding(
        padding: const EdgeInsets.fromLTRB(20, 20, 20, 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,

          children: [
            Expanded(child: Center(child: Image(image: AssetImage("assets/SelfRetportImege.jpg"))), flex: 2,),// "assets/SelfRetportImege.png")),

            Text(widget.questionnaire.instructions),
            SizedBox(height: 40,),

            Text(thisQuestion.title, style: TextStyle(fontWeight: FontWeight.bold),),
            SizedBox(height: 20,),
            Text(thisQuestion.question),

            Expanded(
              flex: 4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(height: 40,),
                  answersPlace(null == thisQuestion.alternativeAnswers ?
                  widget.questionnaire.answers : thisQuestion.alternativeAnswers
                  ),
                  SizedBox(height: 25,),

                  Padding(
                    padding: EdgeInsets.all(0),
                    //padding: EdgeInsets.fromLTRB(30, 40, 30, 45),
                    child: Row(
                      children: [
                        Expanded(
                          child: MyButton(
                              buttonText: "< Previous",
                              contraryColor: true,
                              buttonFnc: (){setState((){questionIndex -= 1;});}
                          ),
                        ),
                        SizedBox(width: 20,),
                        Expanded(
                          child: MyButton(
                              buttonText: "Next >",
                              buttonFnc: (){ widget.questionnaireAns.answers.length > questionIndex ? nextQuestion() : print("..."); }
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
