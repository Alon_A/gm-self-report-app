
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gm_self_report_app/shared/constants.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(title: "Good morning!",),

      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 200),
              Text("Menu",
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              TextButton.icon (
                label: Text("New Self Report", style: TextStyle(color: Colors.black87)),
                onPressed: (){
                  Navigator.pushNamed(context, "/beginTask");
                },
                icon: ImageIcon(
                  AssetImage('assets/addNewTask.png'),
                  size: 50,
                  color: Color(0xff0092C5),
                ),
              ),
              TextButton.icon (
                label: Text("Reports Track", style: TextStyle(color: Colors.black87)),
                onPressed: (){},
                icon: ImageIcon(
                  AssetImage('assets/ReportsTrack.png'),
                  size: 50,
                  color: Color(0xff57C500),
                ),
              ),
              TextButton.icon (
                label: Text("Sessions Track", style: TextStyle(color: Colors.black87)),
                onPressed: (){},
                icon: ImageIcon(
                  AssetImage('assets/SessionsTrack.png'),
                  size: 50,
                  color: Color(0xffA900C5),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
