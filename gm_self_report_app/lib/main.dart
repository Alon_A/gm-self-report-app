import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';

import 'package:gm_self_report_app/models/jsonObjects/questairJson.dart';

import 'package:gm_self_report_app/models/user.dart';
import 'package:gm_self_report_app/screens/home/new_self_report.dart';
import 'package:gm_self_report_app/screens/wrapper.dart';
// import 'package:gm_self_report_app/utils/firebase_auth.dart';

import 'package:gm_self_report_app/screens/home/home_page.dart';
import 'screens/authenticate/sign_in.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    // return MaterialApp( home: Home() );

    // return StreamProvider<User>.value(
    //   //value: AuthService().user,
    //   child: MaterialApp(
    //
    //      home: Wrapper()
    //   ),
    // );


    return MaterialApp(
        routes: {
          '/': (context) => Home(),
          '/beginTask': (context) => PerformSelfReport(),
        }
    );
  }
}










//title: 'Flutter Demo',
// theme: ThemeData(
//   // This is the theme of your application.
//   primarySwatch: Colors.blue,
// ),



// import 'screens/authenticate/sign_in.dart';
// import 'screens/authenticate/register/register.dart';
// import 'package:gm_self_report_app/screens/authenticate/welcome_page.dart';
// import 'package:gm_self_report_app/screens/authenticate/authenticate.dart';

// return MaterialApp(
// routes: {
// '/': (context) => Home(),
// '/authenticate': (context) => Authenticate(),
// '/login': (context) => Login(),
// '/register': (context) => Register(),
// '/setup': (context) => UserSetup(),
// }
// );