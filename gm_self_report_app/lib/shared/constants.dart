import 'package:flutter/material.dart';

const TextStyle TEXT_STYIE = TextStyle(fontSize: 18.0);

class MyButton extends StatelessWidget {

  bool contraryColor = false;

  final Function buttonFnc;
  final String buttonText;
  //final Icon icon;


  MyButton({ this.buttonFnc, this.buttonText, this.contraryColor = false });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      elevation: 0,
      color: contraryColor ? Colors.white : Color(0xff01A0C7),
      shape: RoundedRectangleBorder(side: BorderSide(width : 2, color:Color(0xff01A0C7)),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      // CircleBorder(side: BorderSide(
      //     color: Colors.black, width: 1.0, style: BorderStyle.solid)
      // ),
      minWidth: MediaQuery.of(context).size.width,
      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      onPressed: this.buttonFnc,
      child: Text(this.buttonText,
          textAlign: TextAlign.center,
          style: TEXT_STYIE.copyWith(
              color: contraryColor ? Color(0xff01A0C7) : Colors.white, fontWeight: FontWeight.bold)),
    );
  }
}

class MyTextField extends StatelessWidget {
  final Function onChangedFnc;
  final String hintText;

  MyTextField({ this.onChangedFnc, this.hintText });

  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: true,
      style: TEXT_STYIE,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: hintText,
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      onChanged: onChangedFnc,
    );
  }
}

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;

  MyAppBar({ this.title });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        this.title,
        style: TextStyle(
            color: Color(0XBB000000)
        ),
      ),

      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            //stops: [0, 0.8],
            colors: <Color>[
              Colors.lightBlue[50],
              Colors.white
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(50);
}
