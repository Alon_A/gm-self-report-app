import 'package:flutter/material.dart';

class WaveClipper extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    // TODO: implement getClip
    var path = new Path();

    var startingHeight = size.height / 3;
    var endingHeight = size.height / 2 ;

    var controlHeight = (startingHeight + endingHeight) / 2;
    var controlWidth = size.width / 20;


    path.lineTo(0, startingHeight);    // starting point

    var firstControlPoint = new Offset(controlWidth, controlHeight);
    var firstEndPoint = new Offset(size.width / 2, controlHeight);

    var secondControlPoint = new Offset(size.width - controlWidth, controlHeight);
    var secondEndPoint = new Offset(size.width, endingHeight);

    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);

    path.lineTo(size.width, endingHeight);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper)
  {
    return null;
  }
}