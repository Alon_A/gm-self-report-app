import 'package:flutter/material.dart';

class conction_ERROR extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Oh no!', style: TextStyle(fontWeight: FontWeight.bold)),
            Text("No internet found. Check your connection or try again.")
          ],
        ),
      ),
    );
  }
}
