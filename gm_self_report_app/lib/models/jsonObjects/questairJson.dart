
import 'dart:convert';

// {
// "Questair" : [{ "instructions" : "fjbsflb",
// "numberOptionalInput" : 5,
// "Questions" : ["asd". asd] }
// }

//https://app.quicktype.io/


// {
//   "name" : "base weekly",
//   "uid" : 1,
//   "lang": "en",
//
//   "questionnaires" : [{
//     "name" : "PCL-5",
//     "title": "questionnaire title",
//     "instructions": "bla bla",
//     "uid": 2,
//     "answers": [
//       {
//         "text": "bla bla",
//         "value": 1
//       },
//       {
//         "text": "bla bla",
//         "value": 3
//       },
//       {
//         "text": "bla bla",
//         "value": 5
//       }
//     ],
//   "questions" : [
//   {
//     "title": "question title",
//     "question" : "bla bla",
//     "alternativeAnswers": [
//       {
//         "text": "bla bla",
//         "value": 1
//       },
//       {
//         "text": "bla bla",
//         "value": 3
//       },
//       {
//         "text": "bla bla",
//         "value": 5
//       }
//     ]
//   }
//   ]},
// {
//   "name" : "ABC",
//   "uid": 2,
//   "questions" : []
// }]
// }


// String jsonString = '{  "name" : "base weekly",  "uid" : 1,  "lang": "en",  "questionnaires" : [{    "name" : "PCL-5",    "title": "questionnaire title",    "instructions": "bla bla",    "uid": 2,    "answers": [      {        "text": "bla bla",        "value": 1      },      {        "text": "bla bla",        "value": 3      },      {        "text": "bla bla",        "value": 5      }    ],  "questions" : [  {    "title": "question title",    "question" : "bla bla",    "alternativeAnswers": [      {        "text": "bla bla",        "value": 1      },      {        "text": "bla bla",        "value": 3      },      {        "text": "bla bla",        "value": 5      }    ]  }  ]},{  "name" : "ABC","uid": 2,"questions" : []}]}';

// To parse this JSON data, do
//
//     final questair = questairFromJson(jsonString);
//     final questair = Questair.fromRawJson(jsonString);


class Questair {
  Questair({
    this.name,
    this.uid,
    this.lang,
    this.questionnaires,
  });

  final String name;
  final int uid;
  final String lang;
  final List<Questionnaire> questionnaires;

  factory Questair.fromRawJson(String str) => Questair.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Questair.fromJson(Map<String, dynamic> json) => Questair(
    name: json["name"],
    uid: json["uid"],
    lang: json["lang"],
    questionnaires: List<Questionnaire>.from(json["questionnaires"].map((x) => Questionnaire.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "uid": uid,
    "lang": lang,
    "questionnaires": List<dynamic>.from(questionnaires.map((x) => x.toJson())),
  };
}

class Questionnaire {
  Questionnaire({
    this.name,
    this.title,
    this.instructions,
    this.uid,
    this.answers,
    this.questions,
  });

  final String name;
  final String title;
  final String instructions;
  final int uid;
  final List<Answer> answers;
  final List<Question> questions;

  factory Questionnaire.fromRawJson(String str) => Questionnaire.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Questionnaire.fromJson(Map<String, dynamic> json) => Questionnaire(
    name: json["name"],
    title: json["title"] == null ? null : json["title"],
    instructions: json["instructions"] == null ? null : json["instructions"],
    uid: json["uid"],
    answers: json["answers"] == null ? null : List<Answer>.from(json["answers"].map((x) => Answer.fromJson(x))),
    questions: List<Question>.from(json["questions"].map((x) => Question.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "title": title == null ? null : title,
    "instructions": instructions == null ? null : instructions,
    "uid": uid,
    "answers": answers == null ? null : List<dynamic>.from(answers.map((x) => x.toJson())),
    "questions": List<dynamic>.from(questions.map((x) => x.toJson())),
  };
}

class Answer {
  Answer({
    this.text,
    this.value,
  });

  final String text;
  final int value;

  factory Answer.fromRawJson(String str) => Answer.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Answer.fromJson(Map<String, dynamic> json) => Answer(
    text: json["text"],
    value: json["value"],
  );

  Map<String, dynamic> toJson() => {
    "text": text,
    "value": value,
  };
}

class Question {
  Question({
    this.title,
    this.question,
    this.alternativeAnswers,
  });

  final String title;
  final String question;
  final List<Answer> alternativeAnswers;

  factory Question.fromRawJson(String str) => Question.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Question.fromJson(Map<String, dynamic> json) => Question(
    title: json["title"],
    question: json["question"],
    alternativeAnswers: json["alternativeAnswers"] == null ? null : List<Answer>.from(json["alternativeAnswers"].map((x) => Answer.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "question": question,
    "alternativeAnswers": alternativeAnswers == null ? null : List<dynamic>.from(alternativeAnswers.map((x) => x.toJson())),
  };
}