
import 'dart:convert';

// {
//   "Data": [
//   {"date": 123, "score": 10},
//   {"date": 123, "score": 10}]
// }

class Complete {
  Complete({ this.data });
  final List<Data> data;

  factory Complete.fromRawJson(String str) => Complete.fromJson(json.decode(str));
  String toRawJson() => json.encode(toJson());

  factory Complete.fromJson(Map<String, dynamic> json) => Complete(
    data: List<Data>.from(json["data"].map((x) => Data.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Data {
  Data({
    this.date,
    this.score,
  });

  final String date;
  final int score;

  factory Data.fromRawJson(String str) => Data.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    date: json["date"] == null ? null : json["date"],
    score: json["score"],
  );

  Map<String, dynamic> toJson() => {
    "date": date == null ? null : date,
    "score": score,
  };
}
