
// {
// "uid" : 1,
//
// "questionnaires" : [
//   {
//     "uid": 2,
//     "answers": [1, 2, 3]
//   } ]
// }

import 'dart:convert';

class Answers {
  Answers({
    this.uid,
    this.questionnaires,
  });

  final int uid;
  List<QuestionnaireAns> questionnaires;

  factory Answers.fromRawJson(String str) => Answers.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Answers.fromJson(Map<String, dynamic> json) => Answers(
    uid: json["uid"],
    questionnaires: List<QuestionnaireAns>.from(json["questionnaires"].map((x) => QuestionnaireAns.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "uid": uid,
    "questionnaires": List<dynamic>.from(questionnaires.map((x) => x.toJson())),
  };
}

class QuestionnaireAns {
  QuestionnaireAns({
    this.uid,
    this.answers,
  });

  final int uid;
  List<int> answers;

  factory QuestionnaireAns.fromRawJson(String str) =>
      QuestionnaireAns.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory QuestionnaireAns.fromJson(Map<String, dynamic> json) =>
      QuestionnaireAns(
        uid: json["uid"],
        answers: List<int>.from(json["answers"].map((x) => x)),
      );

  Map<String, dynamic> toJson() =>
      {
        "uid": uid,
        "answers": List<dynamic>.from(answers.map((x) => x)),
      };

  bool checkIfMarked(int index, int value) {
    return answers.length > index ? answers[index] == value : false;
  }

  void markAns(int index, int value)
  {
    if (answers.length == index)
    {
      // add new cell in the ans list
      answers.add(value);
    }
    else if (answers.length > index)
    {
      // Replace ans
      answers[index] = value;
    }
    else
    {
      //ERROR
      print("ERROR");
    }
  }
}
